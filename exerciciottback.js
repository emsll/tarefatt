const DataTypes = require("sequelize");
const sequelize = require("../config/sequelize");
const {Op} = require('sequelize');

//instanciando
const comment_user = async (req, res) => {
  const {id, email} = req.params;
    try {
        const user = await User.findByPk(id);
        const comments = await Comment.findAll(req.body, {
            where: {
                [Op.or]: {
                    userId: {
                        [Op.like]:id,
                    },
                    email: {
                        [Op.like]:'%' + email 
                    },
                },
            },
        });
        if (comments) {
            return res.status(200).json({comments});
        }
        throw new Error();
    }catch (error) {
        return res.status(500).json("Não há comentários para este usuário.");
    }
};

//instanciando
const movies = async (req, res) => {
  const {title, maxEvaluation} = req.body;
    try {
        const movie = await Movie.findAll({
            where: {
                [Op.or]: {
                    title: {
                        [Op.like]:'%' + title + '%'
    
                    },
                    evaluation: {
                        maxEvaluation: {
                            [Op.gte]: 5,
                        },
                    },
                },
            },
        });
        if (movie) {
            return res.status(200).json({movie});
        }
        throw new Error();
    }catch(error) {
        return res.status(500).json("Não foi encontrado filme com classificação menor que 5.");
    }
};

